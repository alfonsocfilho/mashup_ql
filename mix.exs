defmodule MashupQl.MixProject do
  @moduledoc false
  use Mix.Project

  def project do
    [
      app: :mashup_ql,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env()),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [coveralls: :test]
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/fixtures"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :cowboy],
      mod: {MashupQL, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:dialyxir, "~> 0.5", only: :dev, runtime: false},
      {:credo, "~> 0.9.0", only: :dev, runtime: false},
      {:mix_test_watch, "~> 0.6.0", only: :dev, runtime: false},
      {:mock, "~> 0.3.1", only: :test, runtime: false},
      {:ex_doc, "~> 0.18.3", only: :dev, runtime: false},
      {:excoveralls, "~> 0.8.1", only: :test, runtime: false},
      {:plug, "~> 1.5"},
      {:httpoison, "~> 1.0"},
      {:cowboy, "~> 2.2"},
      {:poison, "~> 3.1"}
    ]
  end
end
