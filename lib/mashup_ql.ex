defmodule MashupQL do
  use Application

  alias Plug.Adapters.Cowboy2
  alias MashupQL.Api

  @moduledoc """
  Documentation for MashupQl.
  """

  def start(_type, _args) do
    children = [
      # Define workers and child supervisors to be supervised
      Cowboy2.child_spec(scheme: :http, plug: Api, options: [port: 4001])
    ]

    opts = [strategy: :one_for_one, name: MashupQL.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
