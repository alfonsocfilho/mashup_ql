defmodule MashupQL.Request do
  @moduledoc false
  def request(data) do
    data[:resources]
    |> Enum.map(fn {name, data} ->
      enpoint = data[:endpoint]

      %HTTPoison.Response{status_code: status, headers: headers, body: body} =
        HTTPoison.get!(enpoint)

      {String.to_atom(name),
       %{
         status: status,
         headers: headers |> Enum.into(%{}),
         response: Poison.decode!(body)
       }}
    end)
    |> Enum.into(%{})
  end
end
