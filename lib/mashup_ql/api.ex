defmodule MashupQL.Api do
  @moduledoc """
  Endpoints API
  """
  use Plug.Router

  alias Plug.Conn
  alias MashupQL.Query
  alias MashupQL.Request

  plug(Plug.Logger)
  plug(:match)
  plug(:dispatch)

  post "/query" do
    {:ok, data, _conn_details} = Conn.read_body(conn)

    response =
      data
      |> Query.compile()
      |> Request.request()
      |> Map.put(:__debug__, %{})
      |> Poison.encode!()

    conn
    |> put_resp_header("server", "MashupQL Server")
    |> put_resp_header("content-type", "application/json")
    |> send_resp(200, response)
  end

  get "/health" do
    conn
    |> put_resp_header("server", "MashupQL Server")
    |> send_resp(200, "query")
  end

  match _ do
    send_resp(conn, 404, "Ops! Not found.")
  end
end
