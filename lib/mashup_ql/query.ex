defmodule MashupQL.Query do
  @moduledoc """
  Module for parsing queries
  # TODO: reduce private functions
  """

  @doc """
  Load Server.mql
  """
  def load_server_query do
  end

  defp set_debug_defaults(ast) do
    default_config = Application.get_env(:mashup_ql, :debug)

    Map.merge(%{debug: default_config}, ast)
  end

  defp set_resource_defaults(ast) do
    resource_config = Application.get_env(:mashup_ql, :resource)

    resources =
      ast
      |> Map.get(:resources, %{})
      |> Enum.map(fn {k, v} ->
        {k, Map.merge(resource_config, v)}
      end)
      |> Enum.into(%{})

    Map.put(ast, :resources, resources)
  end

  def compile("") do
    :empty
  end

  def compile(code) do
    {:ok, tokens, _} = code |> to_charlist |> :mashup_ql_lexer.string()
    # IO.puts("\n\nCODE ---------")
    # IO.inspect(code)
    # IO.puts("\n\nTOKENS ---------")
    # IO.inspect(tokens)
    # IO.puts("\n\nAST ---------")

    {:ok, query} = :mashup_ql_parser.parse(tokens)

    final_query =
      query
      |> set_debug_defaults
      |> set_resource_defaults

    post_processing(final_query)
  end

  defp post_processing(ast) do
    ast
    |> prepare_constants
    |> replace_const_resources
    |> clean_ast
  end

  defp prepare_constants(ast) do
    constants = ast[:constants] |> resolve_constants
    ast |> Map.put(:constants, constants)
  end

  defp replace_const_resources(ast) do
    ast
    |> Map.put(
      :resources,
      ast[:resources]
      |> replace_constants(ast[:constants])
    )
  end

  defp clean_ast(ast) do
    ast |> Map.delete(:constants)
  end

  defp replace_value(value, constants) do
    case value do
      {:const, v} -> Map.get(constants, List.to_atom(v), nil)
      _ -> value
    end
  end

  defp replace_constants(resources, constants) do
    resources
    |> Enum.map(fn {k, v} ->
      new_v =
        v
        |> Enum.map(fn {k2, v2} ->
          {k2, replace_value(v2, constants)}
        end)
        |> Enum.into(%{})

      {k, new_v}
    end)
    |> Enum.into(%{})
  end

  defp resolve_constants(nil) do
    %{}
  end

  defp resolve_constants(const) do
    const
    |> Enum.map(fn {k, v} ->
      {k, replace_value(v, const)}
    end)
    |> Enum.into(%{})
  end
end
