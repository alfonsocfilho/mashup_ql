Definitions.

CHAR = [a-zA-Z-_0-9/\.:@]
LETTERS = [a-zA-Z-_0-9@]
WORD = {LETTERS}+
WS = [\000-\s\n]
DIGIT = [0-9]+

Rules.

post      : { token, {'post', TokenLine, 'post'} }.
get       : { token, {'get', TokenLine, 'get' } }.
put       : { token, {'put', TokenLine, 'put' } }.
'{CHAR}+' : S = strip(TokenChars,TokenLen), {token,{string,TokenLine,S}}.
end       : { token, {'end', TokenLine, 'end' } }.
{WS}*     : skip_token.
set       : { token, {'set', TokenLine, TokenChars}}.
where     : { token, {'where', TokenLine, TokenChars}}.
headers   : { token, {'headers', TokenLine, TokenChars}}.
then      : { token, {'then', TokenLine, TokenChars}}.
catch     : { token, {'catch', TokenLine, TokenChars}}.
balance   : { token, {'balance', TokenLine, TokenChars}}.
const     : { token, {'const', TokenLine, TokenChars}}.
false     : { token, {false, TokenLine, list_to_atom(TokenChars)}}.
true      : { token, {true, TokenLine, list_to_atom(TokenChars)}}.
=         : { token, {'equal', TokenLine, TokenChars}}.
\[        : { token, {'list_begin', TokenLine, TokenChars}}.
\]        : { token, {'list_end', TokenLine, TokenChars}}.
\,        : { token, {'comma', TokenLine, TokenChars}}.
\{        : { token, {'object_begin', TokenLine, TokenChars}}.
\}        : { token, {'object_end', TokenLine, TokenChars}}.
\:        : { token, {'colon', TokenLine, TokenChars}}.
{DIGIT}+  : { token, {number, TokenLine, list_to_integer(TokenChars)}}.
{WORD}    : { token, {'word', TokenLine, TokenChars}}.


Erlang code.

strip(TokenChars,TokenLen) ->
  lists:sublist(TokenChars, 2, TokenLen - 2).
