Nonterminals
document resources resource kv kvs verbs verb block blocks block_name value
expression expressions kvs_n kv_n value_block constants constant values list
object object_kv object_kvs.


Terminals
post get put word end set where headers balance then catch equal string number
true false const list_begin list_end comma colon object_begin object_end.


Rootsymbol document.

document -> expressions : group_resources('$1').

expressions -> expression : ['$1'].
expressions -> expression expressions : ['$1'] ++ '$2'.

expression -> resources : { 'resources',  '$1' }.
expression -> constants : { 'constants',  '$1' }.

constants -> constant : '$1'.
constants -> constant constants : maps:merge('$1', '$2').
constant -> const word equal value : #{ list_to_atom(get_value('$2')) => '$4' }.

resources -> resource : '$1'.
resources -> resource resources : maps:merge('$1', '$2').

resource -> verbs word blocks end : #{ list_to_binary(get_value('$2')) => maps:merge(#{ method => get_token('$1')}, '$3') }.
resource -> verbs word end : #{ list_to_binary(get_value('$2')) => #{ method => get_token('$1') } }.

% Resources verbs
verbs -> verb : '$1'.

verb -> post : '$1'.
verb -> get : '$1'.
verb -> put : '$1'.

% Block content
blocks -> block : '$1'.
blocks -> block blocks : maps:merge('$1', '$2').

block -> block_name end : #{}.
block -> block_name kvs end : get_block_content('$1', '$2').

block_name -> set : { '$1', 'inline', 'set'}.
block_name -> where : { '$1', 'block', 'params'}.
block_name -> headers : { '$1', 'block', 'headers'}.
block_name -> then : { '$1', 'block', 'then'}.
block_name -> catch : { '$1', 'block', 'catch'}.

% Key-value blocks
kvs -> kv : '$1'.
kvs -> kv kvs :  maps:merge('$1', '$2').
kv -> word equal value : #{ list_to_atom(get_value('$1')) => '$3' }.

% Value/Data types
value -> string : list_to_binary(get_value('$1')).
value -> number : get_value('$1').
value -> true : get_value('$1').
value -> false : get_value('$1').
value -> list : '$1'.
value -> object : '$1'.
value -> value_block : '$1'.
value -> word : { 'const', get_value('$1')}.

list -> list_begin list_end : [].
list -> list_begin values list_end: '$2'.

object -> object_begin object_end : #{}.
object -> object_begin object_kvs object_end : '$2'.

object_kvs -> object_kv : '$1'.
object_kvs -> object_kv comma object_kvs : maps:merge('$1', '$3').
object_kv -> word colon value : #{ list_to_atom(get_value('$1')) => '$3' }.

values -> value : [ '$1' ].
values -> value comma values : [ '$1' | '$3' ].

% Balance block
value_block -> balance kvs_n end : '$2'.
% kvs_n -> kv_n : maps:merge(#{ type => 'balance' }, '$1').
kvs_n -> kv_n : '$1'.
% kvs_n -> kv_n kvs_n : maps:merge(#{ type => 'balance' }, maps:merge('$1', '$2')).
kvs_n -> kv_n kvs_n : { 'balance', maps:merge('$1', '$2') }.
kv_n -> number equal string : #{ list_to_binary(get_value('$3')) => get_value('$1') }.


Erlang code.

group_resources(L) -> lists:foldl(fun({T, V}, A) ->
  case T of
    'resources' -> maps:put('resources', maps:merge(maps:get('resources', A, #{}), V), A);
    'constants' -> maps:put('constants', maps:merge(maps:get('constants', A, #{}), V), A)
  end
end, #{}, L).

get_token({A, _, _}) -> A.

get_value({_, _, V}) -> V.

get_block_content({ _A, T, N}, C) ->
  case T of
    'block' ->
      #{ N =>  maps:to_list(C) };
    'inline' ->
      C
  end.
