# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :mashup_ql,
  port: 8001,
  debug: %{
    log: false,
    benchmark: false,
    graph: false,
    trace: false,
    discover: false,
    query: false
  },
  resource: %{
    timeout: 1000,
    cache: 1200,
    ignore_error: false,
    protocol: :http,
    backpressure: 50,
    hidden: false,
    async: false,
    retry: 0,
    job_life: 3600,
    job_prefix: ''
  }

if Mix.env() == :dev do
  config :mix_test_watch, clear: true
end

# This configuration is loaded before any dependency and is restricted
# to this project. If another project depends on this project, this
# file won't be loaded nor affect the parent project. For this reason,
# if you want to provide default values for your application for
# 3rd-party users, it should be done in your "mix.exs" file.

# You can configure your application as:
#
#     config :mashup_ql, key: :value
#
# and access this configuration in your application as:
#
#     Application.get_env(:mashup_ql, :key)
#
# You can also configure a 3rd-party app:
#
#     config :logger, level: :info
#

# It is also possible to import configuration files, relative to this
# directory. For example, you can emulate configuration per environment
# by uncommenting the line below and defining dev.exs, test.exs and such.
# Configuration from the imported file will override the ones defined
# here (which is why it is important to import them last).
#
#     import_config "#{Mix.env}.exs"
