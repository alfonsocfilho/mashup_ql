defmodule MashupQL.QueryTest do
  @moduledoc false
  use ExUnit.Case
  alias MashupQL.Query
  doctest MashupQL.Query

  # @tag :skip
  describe "Data Types" do
    setup [:before_each]

    test "should support strings", context do
      code = "
      const value = 'string'

      get Test
        set
          mock = value
        end
      end
      "

      expected = make_expected(%{mock: "string", method: :get}, context)
      compiled = Query.compile(code)
      assert is_binary(compiled[:resources]["Test"][:mock])
      assert compiled == expected
    end

    test "should set numbers", context do
      code = "
      const value = 123

      get Test
        set
          mock = value
        end
      end
      "

      expected = make_expected(%{mock: 123, method: :get}, context)
      compiled = Query.compile(code)
      assert is_number(compiled[:resources]["Test"][:mock])
      assert compiled == expected
    end

    test "should set boolean - true", context do
      code = "
      const value = true

      get Test
        set
          mock = value
        end
      end
      "

      expected = make_expected(%{mock: true, method: :get}, context)
      compiled = Query.compile(code)
      assert is_boolean(compiled[:resources]["Test"][:mock])
      assert compiled == expected
    end

    test "should set boolean - false", context do
      code = "
      const value = false

      get Test
        set
          mock = value
        end
      end
      "

      expected = make_expected(%{mock: false, method: :get}, context)
      compiled = Query.compile(code)
      assert is_boolean(compiled[:resources]["Test"][:mock])
      assert compiled == expected
    end

    test "should set empty array", context do
      code = "
      const value = []

      get Test
        set
          mock = value
        end
      end
      "

      expected = make_expected(%{mock: [], method: :get}, context)

      compiled = Query.compile(code)
      assert is_list(compiled[:resources]["Test"][:mock])
      assert compiled == expected
    end

    test "should set array", context do
      code = "
      const value = [1, [ false], true, 2 , 'a']

      get Test
        set
          mock = value
        end
      end
      "

      expected =
        make_expected(
          %{
            mock: [1, [false], true, 2, "a"],
            method: :get
          },
          context
        )

      compiled = Query.compile(code)
      assert is_list(compiled[:resources]["Test"][:mock])
      assert compiled == expected
    end

    test "should set object", context do
      code = "
      const value = { key: 'value' }

      get Test
        set
          mock = value
        end
      end
      "

      expected =
        make_expected(
          %{
            mock: %{key: "value"},
            method: :get
          },
          context
        )

      compiled = Query.compile(code)
      assert is_map(compiled[:resources]["Test"][:mock])
      assert compiled == expected
    end

    # test "should set object"
  end

  #
  # @tag :skip
  # describe "QL Pipes" do
  #   test "should declare basic resource"
  # end

  @tag :skip
  describe "QL Resources" do
    setup [:before_each]

    test "should be empty" do
      assert Query.compile("") == :empty
    end

    #
    # @tag :skip
    # test "should declare basic resource block", context do
    #   code = "post Hello end"
    #
    #   expected = %{
    #     resources: %{
    #       "Hello" =>
    #         Map.merge(context[:base_resource], %{
    #           method: :post
    #         })
    #     },
    #     debug: context[:debug_data]
    #   }
    #
    #   assert Query.compile(code) == expected
    # end

    test "should set complex resource", context do
      code = "
const myVar = 'test'
const myVar2 = 'test2'
const newVar = myVar

get New
  set
    endpoint = balance
      50 = 'api-1'
      50 = 'api-2'
    end
  end
end

const myVar3 = 3000

post Hello
  set
    endpoint = 'api'
    timeout = 100
    cache = myVar3
  end

  headers
    Content-Type = 'text/plain'
  end

  where
    code = 2
  end

  then
    status = 200
  end

  catch
    status = 500
  end
end
"

      expected = %{
        resources: %{
          "New" =>
            Map.merge(context[:base_resource], %{
              method: :get,
              endpoint: {:balance, %{"api-1" => 50, "api-2" => 50}}
            }),
          "Hello" =>
            Map.merge(context[:base_resource], %{
              method: :post,
              endpoint: "api",
              timeout: 100,
              cache: 3000,
              headers: [
                "Content-Type": "text/plain"
              ],
              params: [
                code: 2
              ],
              then: [
                status: 200
              ],
              catch: [
                status: 500
              ]
            })
        },
        debug: context[:debug_data]
      }

      assert Query.compile(code) == expected
    end

    # test "should set endpoint"
    # test "should set alias"
  end

  defp before_each(_context) do
    {:ok,
     [
       base_resource: %{
         timeout: 1000,
         cache: 1200,
         ignore_error: false,
         protocol: :http,
         backpressure: 50,
         hidden: false,
         async: false,
         retry: 0,
         job_life: 3600,
         job_prefix: ''
       },
       debug_data: %{
         log: false,
         benchmark: false,
         graph: false,
         trace: false,
         discover: false,
         query: false
       }
     ]}
  end

  defp make_expected(value, context) do
    %{
      resources: %{
        "Test" => Map.merge(context[:base_resource], value)
      },
      debug: context[:debug_data]
    }
  end

  # defp typeof(self) do
  #   cond do
  #     is_float(self) -> "float"
  #     is_number(self) -> "number"
  #     is_atom(self) -> "atom"
  #     is_boolean(self) -> "boolean"
  #     is_binary(self) -> "binary"
  #     is_function(self) -> "function"
  #     is_list(self) -> "list"
  #     is_tuple(self) -> "tuple"
  #     true -> "idunno"
  #   end
  # end

  # @tag :skip
  # describe "QL Debug" do
  #   test "cac"
  # end

  #   test "should match" do
  #     code = "
  # post Hello
  #   set
  #     endpoint = 'teste'
  #   end
  # end"
  #
  #     expected = [
  #       resources: %{
  #         "Hello" => [
  #           method: :post,
  #           endpoint: "teste"
  #         ]
  #       }
  #     ]
  #
  #     assert Parser.parse(code) == expected
  #   end
  #
  #   @tag :skip
  #   test "greets the world" do
  #     codebkp = "
  # post Hero
  #   set
  #     endpoint = 'novo'
  #     number = 8676
  #     ignore = true
  #     timeout = long
  #   end
  #
  #   where
  #     id = ttas
  #   end
  # end"
  #
  #     Parser.parse(codebkp)
  #     assert :ok == false
  #   end
end
