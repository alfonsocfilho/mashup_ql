defmodule MashupQL.ApiTest do
  @moduledoc false

  use ExUnit.Case, async: true
  use Plug.Test

  import Mock

  alias MashupQL.Api, as: Api

  doctest Api

  @opts Api.init([])

  setup_with_mocks([
    {HTTPoison, [],
     [
       get!: fn url ->
         IO.inspect(url)

         result =
           case url do
             "http://example.com" -> %{ok: "complete"}
             "http://example.com/@id" -> %{ok: "complete id"}
             _ -> %{error: "no match"}
           end

         %HTTPoison.Response{
           status_code: 200,
           body: Poison.encode!(result)
         }
       end
     ]}
  ]) do
    :ok
  end

  @tag :skip
  test "should run basic query" do
    conn = conn(:post, "/query", Fixtures.basic())

    conn = Api.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200

    assert conn.resp_body |> Poison.decode!() == %{
             "Test" => %{
               "status" => 200,
               "response" => %{
                 "ok" => "complete"
               },
               "headers" => %{}
             },
             "__debug__" => %{}
           }
  end

  @tag :skip
  test "should make multiple requests" do
    conn = conn(:post, "/query", Fixtures.multiple_request())

    conn = Api.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200

    assert conn.resp_body |> Poison.decode!() == %{
             "Test" => %{
               "status" => 200,
               "response" => [
                 %{
                   "ok" => "complete"
                 },
                 %{
                   "ok" => "complete"
                 }
               ],
               "headers" => %{}
             },
             "__debug__" => %{}
           }
  end
end
