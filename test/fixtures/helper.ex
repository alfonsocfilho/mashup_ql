defmodule Fixtures do
  @moduledoc false

  @basic """
  const test = 1

  get Test
    set
      endpoint = 'http://example.com'
    end
  end
  """
  def basic, do: @basic

  @multiple_request """
  get test
    set
      endpoint = 'http://example.com/@id'
    end

    where
      @id = [1, 2, 3]
    end
  end
  """
  def multiple_request, do: @multiple_request
end
