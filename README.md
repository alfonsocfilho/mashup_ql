# MashupQl

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `mashup_ql` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:mashup_ql, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/mashup_ql](https://hexdocs.pm/mashup_ql).


# Request Flow

```
Client.mql ---> Server.mql ---> Response
```

# Endpoints

- `/query`
- `/query/<query-id>`
- `/health`
- `/job/<id>`

# Mashup Query Language

## Comments

```
# single line comment

###
multi line
comment
###
```

## Constants
```
const MyNumber = 0
const MyFloat = 1.5
const MyBoolean = true
const MyString = "string"
const MyArray = [1, 2, 3, 4]
const MyObject = {a:1, b:2, c:3, d:4}
```

## Pipes
Pipes are like pure functional functions.
```
# pipe without arguments
def MyPipe()
  pick("key")
  |> toUpperCase
end

# without parentesis alternaty
def MyPipe
  pick("key")
  |> toUpperCase
end

# pipe with arguments
def MyPipe(key, delimiter)
  pick(key)
  |> split(delimiter)
  |> toUpperCase
end

```
## `debug` block
Configure how server should
```
debug
  log       # enable log messages
  benchmark # enable benchmark logs
  graph     # display resources dependency graph
  trace     # display a detailed timeline of the request
  discover  # display resources list
  query     # display computed query (merged)
end
```

## Resource block
Declare a endpoint/api resource to be consumed.
```

# simplest resource
get Heroes
end

# with alias
get Heroes as H
end

# extending other resources
get Heroes extends Characters
end

# locking resource to be extended
final get Heroes
end

# resource not consumed, defined only
# to be extended
abstract get Heroes
end

# everything toghether
final get Heroes as H extends Characters
end
```

## `set` block
Configure how resource should behave
```
set {
  # define resource endpoint
  endpoint = "api.example.com"

  # with load balance
  endpoint = loadbalance
    api-1.example.com = 50
    api-2.example.com = 50
  end

  health = api.example.com/health

  # time to wait until endpoint respond
  timeout = 1000

  # time to cache endpoint response
  cache = 1000

  # ignore codes 5xx
  ignore-fail = false

  # http http quic
  protocol = http
  backpressure = 50
  hidden = false
  async = true
  retry = 2 # how many time must retry until fail request
}

```

## Example
```
# comment

# constants
const MyConst = "name"

# pipes/function
def MyPipe()
  pick
  |>  toUpperCase
end

# debug settings
debug
  log # print log messages
  benchmark # print benchmark
  graph # print resources dependency graph
  trace # print timeline
  discover # print available function/variables and resources
  query # print final query (client and server merged)
end

# need?
#auth {
#  key = bds9sADVadv98adnv98dsv98sbvd9BASDV
#}

default
  set
  end
end

post JobCron as Job extends Cron

  set
    final endpoint = "api.example.com/@id"
    final timeout = 1000
    cache = default 1200
      min = 1000
      max = 2000
    end
    ignore-error = false
    protocol = http # http | http2 | quic
    backpressure = 50
    hidden = false
    async = true
    retry = 0
    job-life = 3600
    job-prefix = "my-job" # my-job-basdv7ashdvsdvna9vhn
  end

  headers
    Autorization = affbasb245145
  end

  where
    @id = [12,234,23,44]
    name = "adasd"
    name = [1, 2, 3, 4]
    name <- otherResource
    name <- [1, 2, 3, 4]
  end

  then
    status = 200
    headers = []
    content = pipeFunction
    encode = json
    compress = gzip
  end

  catch
    status = 200
    headers = []
    content = pipeFunction
    encode = json # json | bson | msgpack | json-ld
    compress = gzip # gzip | deflate | brotli
  end
end


```




----------------


Objects

  Keys

  Values

  Slice

Arrays

  Map

  Reduce

  Filter

  Join

  Flatten

  Head

  Tail

  Slice

Logical

  And

  Or

  If

  IfElse

Math

  Add

  Sub

  Sum

String

  Lowercase

  Uppercase

  Split

  Match

  Slice

Funcions

  Log

  Time

----------

```
[
  resources: [
    'Heros': [
      alias: 'H',
      endpoint: ''
      timeout: ''

    ]
  ]
]
```
